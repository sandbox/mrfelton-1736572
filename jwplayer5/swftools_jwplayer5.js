/**
 * Ensure swftools namespace is established.
 */
var swftools = swftools || {};

/**
 * Establish jwPlayerReady namespace.
 */
swftools.jwPlayerReady = swftools.jwPlayerReady || {};

/**
 * Callback from LongTail player when an instance is initialized.
 */
function playerReady(player) {
  swftools.jwplayer5.pushInstance(player.id);
  player = swftools.getObject(player.id);

  // Attach listeners and handlers to the player
  jQuery.each(swftools.jwPlayerReady, function() {
    this(player);
  });

};


/**
 * Attach state tracking function to auto-pause players
 */
swftools.jwPlayerReady.swftools_jwplayer5 = function(player) {
  player.addModelListener('STATE','swftools.jwplayer5.stateTracker');
};


/**
* Define LongTail player functions here.
*/
swftools.jwplayer5 = function() {
  var instances = [];
  var activePlayer = null;
  return {
    pushInstance: function(playerID) {
      instances.push(playerID);
    },
    stateTracker: function(player) {
      // States are: IDLE, BUFFERING, PLAYING, PAUSED, COMPLETED
      if (player.newstate == 'PAUSED' || player.newstate == 'COMPLETED') {
        if (player.id == activePlayer) {
          activePlayer = null;
        }
      }
      // Pause other players when another one starts
      if (player.newstate == 'PLAYING') {
        if (activePlayer && player.id != activePlayer) {
          swftools.jwplayer5.pause(activePlayer);
        }
        activePlayer = player.id;
      }
    },
    play: function(playerID) {
      swftools.getObject(playerID).sendEvent('PLAY', 'true');
    },
    pause: function(playerID) {
      swftools.getObject(playerID).sendEvent('PLAY', 'false');
    },
    stop: function(playerID) {
      swftools.getObject(playerID).sendEvent('STOP');
    },
    mute: function(playerID) {
      swftools.getObject(playerID).sendEvent('MUTE', 'true');
    },
    unmute: function(playerID) {
      swftools.getObject(playerID).sendEvent('MUTE');
    }
  }
}();


/**
 * Define behaviors that interact with the players.
 */
(function ($) {

Drupal.behaviors.swftools_jwplayer5 = {

  attach: function(context) {

    $('[class^=jwplayer5-accessible]:not(.swftools-jwplayer5-processed)', context).addClass('swftools-jwplayer5-processed').each(function () {
      $(this).click(function(){
        var classes = $(this).attr('class');
        var classes = classes.split(' ');
        var parts = classes[0].split('-');
        var idStarts = 22 + parts[2].length;
        var action = "swftools.jwplayer5." + parts[2] + "('" + classes[0].substring(idStarts) + "')";
        eval(action);
        return false;
      });
    });
  }
};
})(jQuery);
