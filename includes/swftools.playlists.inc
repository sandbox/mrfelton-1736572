<?php

/**
 * @file
 * Implements SWF Tools support for playlists.
 *
 * The functions in this file provide the playlist capabilities for SWF Tools.
 * They are separated in to this include file for convenience.
 */

/**
 * Prepares an array of filenames, or file objects, for use in a playlist.
 */
function swftools_prepare_playlist_data(&$element, $playlist_children) {

  // Initialise two flags for establishing the action to be taken on this playlist
  $action = '';
  $mixed_media = FALSE;

  // Iterate over the play list array to 'standardise' it irrespective of source
  foreach ($playlist_children AS $key) {

    // If content on this key is an object then convert it to an array
    if (is_object($element['#playlist'][$key])) {
      $element['#playlist'][$key] = (array)$element['#playlist'][$key];
    }

    // If this element is a string then make it an array with this value on the file key
    if (!is_array($element['#playlist'][$key])) {
      $element['#playlist'][$key] = array(
        'file' => $element['#playlist'][$key],
      );
    }

    /**
     * If #field_map is set then the playlist has data in it that can be used
     * to build a standard SWF Tools playlist. The file mapping is required,
     * but other mappings are optional depending on the data available from the
     * incoming field.
     */
    if (isset($element['#field_map'])) {
      $field_map = array(
        'file',
        'title',
        'image',
        'description',
        'author',
        'date',
        'link',
        'duration',
        'stream',
      );
      foreach ($field_map as $map) {
        if (isset($element['#field_map'][$map])) {
          $element['#playlist'][$key][$map] = $element['#playlist'][$key][$element['#field_map'][$map]];
        }
        else {
          $element['#playlist'][$key][$map] = NULL;
        }
      }
    }

    /**
     * Initialise other element keys as NULL to make checks in other modules easy.
     */
    $element['#playlist'][$key] += array(
      'title' => NULL,
      'image' => NULL,
      'description' => NULL,
      'author' => NULL,
      'date' => NULL,
      'link' => NULL,
      'duration' => NULL,
      'stream' => FALSE,
    );

    // Expand uri if necessary, provided this isn't already declared a stream
    if (!$element['#playlist'][$key]['stream']) {
      $element['#playlist'][$key]['file'] = swftools_build_uri($element['#playlist'][$key]['file']);
    }

    // Find out if this item is a stream by checking for rtmp and exploding it
    if (file_uri_scheme($element['#playlist'][$key]['file']) == 'rtmp') {
      $stream = explode(' ', $element['#playlist'][$key]['file']);

      // If the filepath exploded then assume we have a valid stream
      if (count($stream) == 2) {
        list($element['#playlist'][$key]['stream'], $element['#playlist'][$key]['file']) = $stream;
        // @todo Why did we set a flag on othervars when we found a stream?
        // $element['#othervars']['stream'] = TRUE;

        // @todo Action checking fails as streams don't have an extension - assume mixed media
        $element['#action'] = $element['#action'] ? $element['#action'] : 'media_list';
      }
    }

    // Expand image uri if necessary
    if ($element['#playlist'][$key]['image']) {
      $element['#playlist'][$key]['image'] = swftools_build_uri($element['#playlist'][$key]['image']);
    }

    // Allow other modules to modify this playlist element (e.g. getID3)
    drupal_alter('swftools_playlist_element', $element['#playlist'][$key]);

    // If the caller wants us to work out the action for them then it happens in here
    if (!$element['#action']) {

      // Get the extension for this item
      $extension = strtolower(pathinfo($element['#playlist'][$key]['file'], PATHINFO_EXTENSION));

      // Only try to determine actions if there's an extension to work with, and we didn't already work out it's mixed
      if ($extension && !$mixed_media) {

        // Work out what we'd do with this file
        $action_for_this_file = swftools_get_action('dummy.' . $extension);

        // If this is the first file we've processed we log it
        if (!$action) {
          $action = $action_for_this_file;
        }

        // Is this action the same as the first file we saw? If not we have mixed media
        if ($action != $action_for_this_file) {
          $mixed_media = TRUE;
          $action = 'media_list';
        }

      }

    }

  }

  // If we didn't get an action (happens with streams as they have no extension) then specify an action now
  $action = $action ? $action : 'media_list';

  // Pluralize the action for multiple files if not already pluralized
  $action = (substr($action, -5, 5) == '_list') ? $action : $action . '_list';

  // If the called didn't specify an action then assign it now
  $element['#action'] = $element['#action'] ? $element['#action'] : $action;

  // Call drupal_alter to let other modules modify the entire playlist if they want
  drupal_alter('swftools_playlist', $element);

}

/**
 * Fetches an xml playlist from the {cache_swftools} table.
 *
 * SWF Tools no longer generates a physical file for the playlist. Instead it
 * places an entry in its internal cache table and then serves the file from
 * there. Pages request the files by accessing swftools/playlist/nnn, where
 * nnn is the cid of the content.
 *
 * @param string $cid
 *   The cache id of the playlist being requested.
 *
 */
function swftools_get_xml($cid) {
  $cid = substr($cid, 0, -4);
  if ((!$data = cache_get($cid, 'cache_swftools')) || !$data->data['#attached']['xml'][0]) {
    return MENU_NOT_FOUND;
  }
  else {
    drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');
    print $data->data['#attached']['xml'][0];
    drupal_exit();
  }
}
