<?php

/**
 * @file
 * Configuration settings for SWF Tools.
 */

/**
 * Load the core module that provides common code to both the API and the full
 * SWF Tools module. Call it using both swfools and swftools_api as the module
 * name as we don't know which is enabled.
 */
module_load_include('inc', 'swftools', 'includes/swftools.core');
module_load_include('inc', 'swftools_api', 'includes/swftools.core');

/**
 * Form builder: Standard embedding settings.
 */
function swftools_admin_embed_form($form, &$form_state) {

  // Attach the embed method selector
  $form['swftools_embed_method'] = swftools_embed_method_selector();

  // Always add JavaScript?
  $form['swftools_always_add_js'] = array(
    '#type' => 'radios',
    '#title' => t('Add JavaScript to all pages'),
    '#default_value' => variable_get('swftools_always_add_js', SWFTOOLS_ALWAYS_ADD_JS),
    '#options' => array(
      0 => t('Disabled'),
      1 => t('Enabled (recommended)'),

    ),
    '#description' => t('Modules such as filters result in caching of the mark up generated
                         to display Flash content. In these cases SWF Tools will not be
                         triggered to add the necessary JavaScript to the page and the Flash
                         content will not display. Normally you should leave this setting
                         enabled. Setting it to disabled may prevent content from appearing.'),
  );

  // JavaScript location?
  $form['swftools_javascript_location'] = array(
    '#type' => 'radios',
    '#title' => t('JavaScript placement'),
    '#default_value' => variable_get('swftools_javascript_location', SWFTOOLS_JAVASCRIPT_INLINE),
    '#options' => array(
      SWFTOOLS_JAVASCRIPT_INLINE => t('Inline'),
      SWFTOOLS_JAVASCRIPT_HEADER => t('Header'),
      SWFTOOLS_JAVASCRIPT_FOOTER => t('Footer'),
    ),
    '#description' => t('Scripts to activate JavaScript embedding can be placed inline with
                         the page markup, or placed in the page header or footer. If you are
                         using an input filter to generate content you should <em>not</em>
                         put the JavaScript in the header or footer as it will not be cached
                         and the content will not display. If you are using CCK then script
                         can safely be placed in the header or footer if you prefer to avoid
                         putting JavaScript within the page body.'),
  );

  // SWF Tools cache enabled?
  // @todo Move this to a proper location, or remove it?
  $form['swftools_cache_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('SWF Tools cache'),
    '#default_value' => variable_get('swftools_cache_enabled', 1),
    '#options' => array(
      1 => t('Enabled'),
      0 => t('Disabled'),
    ),
    '#description' => t('When the SWF Tools cache is enabled then content that has already
                         been processed by SWF Tools will be retrieved from the cache. This
                         gives a significant performance boost, but during development it is
                         helpful to be able to disable the cache and ensure that the content
                         is rendered on each page call.'),
  );

  // Add custom form handler to flush cache upon submit
  $form['#submit'][] = 'swftools_admin_settings_submit';

  // Return the form
  return system_settings_form($form);

}


/**
 * Returns a set of radio buttons to allow the embedding method to be selected.
 */
function swftools_embed_method_selector() {

  // Get the available embedding methods
  $methods = swftools_get_methods('embed');

  // Build array of options ready for the selector
  if (count($methods)) {

    foreach ($methods AS $method => $info) {

      // Assume the method is not private and that it has no accompanying library
      $info += array(
        'private' => FALSE,
        'library' => '',
      );

      // Only show this method as an embedding method if it is not a private method
      if (!$info['private']) {

        // Start with the name of the method
        $swf_embed_methods[$method] = $info['title'];

        // If the required shared file is missing add a message explaining
        if ($info['library'] && !file_exists($info['library'])) {

          // Only set a form error if the error is OTHER THAN SWF Object2 as
          // the SWF Object 2 module can access the remote library
          if ($info['name'] != 'swftools_swfobject2') {

            // Set an error on the form so the field set expands and highlights the error
            form_set_error('swftools_embed_method', t('The shared file for @method is missing.', array('@method' => $info['title'])));

          }

          // Add missing message to the form
          // TODO: Could make a customised message for SWF Object 2 missing
          $swf_embed_methods[$method] = $swf_embed_methods[$method] . ' - Missing '. $info['library'];

          // If download data is available then add a download link
          if ($info['download']) {
            $swf_embed_methods[$method] = $swf_embed_methods[$method] . ' - ' . l(t('Download here'), $info['download']);
          }
        }
      }
    }
  }

  return array(
    '#type' => 'radios',
    '#title' => t('Embedding method'),
    '#default_value' => variable_get('swftools_embed_method', 'swftools_direct'),
    '#options' => $swf_embed_methods,
    '#description' => t('Flash content can be embedded on a page directly, using &lt;object&gt; tags, or
                       JavaScript can be used to dynamically replace content on the page with the Flash
                       movie. SWF Tools supports direct embedding without installing any additional
                       modules, but JavaScript methods require that a supporting module is enabled, and
                       may require a JavaScript library to be downloaded. JavaScript interaction between
                       the page and the Flash movie is possible with both direct embedding and
                       JavaScript methods. Note that the direct embedding method does not support the Flash
                       express install feature.'),
  );

}

function swftools_admin_players_form($form, &$form_state) {

  // Get players form by calling the profile form with no profile setting
  $form = swftools_players_profile_form();

  // @todo Rename swftools_handlers to swftools_players in .install

  // Make a fieldset
  $form['swftools_players'] += array(
    '#type'  => 'fieldset',
    '#title' => t('Default players'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

//  // Get available actions - this is just a list of available actions and descriptions about them
//  $actions = swftools_get_actions();
//
//  // Get the array of actions, keyed on extension
//  $_extensions = _swftools_actions();
//
//  // Turn it around to we are organised by action
//  foreach ($_extensions as $extension => $action) {
//    $extensions[$action][] = $extension;
//  }
//
//  // Make a fieldset
//  $form['swftools_advanced'] = array(
//    '#type'  => 'fieldset',
//    '#title' => t('File associations'),
//    '#collapsible' => FALSE,
//    '#collapsed' => FALSE,
//  );
//
//  // Make extensions an array
//  $form['swftools_advanced']['swftools_extensions'] = array(
//    '#tree' => TRUE,
//  );
//
//  // Put each option on the form to let the user assign extensions to each available action
//  foreach ($actions as $action => $info) {
//
//    // Only output single file handlers (list handlers handle collections of single files of the same type)
//    if(substr($action, -5) != '_list') {
//
//      // Place an option on the page
//      $form['swftools_advanced']['swftools_extensions'][$action] = array(
//        '#type' => 'textfield',
//        '#title' => t('Extensions to associate with ' . $info['#type']),
//        '#default_value' => isset($extensions[$action]) ? implode(' ', $extensions[$action]) : '',
//        '#description' => t('Enter a list of extensions, separated by spaces, that SWF Tools should associate with ' . $info['#type'] . '.'),
//      );
//
//    }
//
//  }

  // Add custom form handler to flush cache upon submit
  $form['#submit'][] = 'swftools_admin_settings_submit';

  // Add custom handler to process extension handling
//  $form['#submit'][] = 'swftools_admin_extensions_submit';

//  // Add custom handler to process extension handling
//  $form['#submit'][] = 'swftools_admin_mime_type_submit';

  // Return finished form
  return system_settings_form($form);

}

function swftools_admin_players_options($action, $description, $profile = '') {

  $methods = swftools_get_methods($action);

  if (count($methods)) {
    foreach ($methods AS $method => $info) {

      if ($info['library'] && !file_exists($info['library'])) {
        $list[$method] = $info['title'] . ' - Missing '. $info['library'];
        if (isset($info['donwload']) && $info['download']) {
          $list[$method] = $list[$method] . ' - ' . l(t('Download here'), $info['download']);
        }
      }
      else {
        $list[$method] = $info['title'];
      }
    }
  }

  // If there are no handlers reporting then return now
  if (!$list) {
    return;
  }

  // Sort the list of methods
  asort($list);

  // 'None' is always an option, unless the action is 'swf'
  if ($action != 'swf') {
    $list = array(t('None')) + $list;
  }

  // Determine the current default for this action, using this profile
  $default = swftools_get_player($action, $profile);

  return array(
    '#type' => 'radios',
    '#title' => $description,
    '#default_value' => $default ? $default : '0',
    '#options' => $list,
  );

}

/**
 * Returns a form definition for the profile players page.
 */
function swftools_players_profile_form($profile = '') {
  $actions = swftools_get_actions(TRUE);
  foreach ($actions as $action => $info) {
    if ($form_element = swftools_admin_players_options($action, $info['#description'], $profile)) {
      $form['swftools_players'][$action] = $form_element;
    }
  }
  $form['swftools_players']['#tree'] = TRUE;
  return $form;
}

///**
// * Returns an array of mime types, keyed by file extension.
// *
// * @todo Does this need to be in the main module, or just in admin?
// */
//function _swftools_mime_types() {
//
//  $defaults = array(
//    'swf' => 'application/x-shockwave-flash',
//    'flv' => 'video/x-flv',
//    'mp3' => 'audio/mpeg',
//    'jpg' => 'image/jpeg',
//    'jpeg' => 'image/jpeg',
//    'jpe' => 'image/jpeg',
//    'png' => 'image/png',
//    'gif' => 'image/gif',
//
//  );
//
//  // Use settings from configuration page, or defaults
//  $mime_types = variable_get('swftools_mime_types', $defaults);
//
//  // Return the result
//  return $mime_types;
//
//}


/**
 * Explodes a string of file extensions and converts them back in to an array.
 *
 * On the admin page we show the user a list of actions and allow them to
 * associate extensions with each one. But when it comes to generating content
 * we will have the extension and we need to discover the action.
 *
 * What we do in this submit handler is crunch the settings array so it is
 * available with extensions as key under the variable swftools_actions. So
 * we actually store this data twice,
 */
function swftools_admin_extensions_submit($form, &$form_state) {
// @todo Test
  // Explode the separate list back to an array of strings
  foreach ($form_state['values']['swftools_extensions'] as $action => $extensions) {

    // Explode in to separate pieces
    $temp = explode(' ', $extensions);

    // Trim white space
    $temp = array_map('trim', $temp);

    // Unset empty elements
    $temp = array_filter($temp);

    // Store the result
    $form_state['values']['swftools_extensions'][$action] = $temp;

  }

  // For use in a page it's easier to have things organised by extension
  $actions = array();

  foreach($form_state['values']['swftools_extensions'] as $action => $extensions) {
    $actions += array_fill_keys($extensions, $action);
  }

  // Attach array of extension - action pairs to swftools_actions ready for saving
  $form_state['values']['swftools_actions'] = $actions;

}




/**
 * Moves form elements into fieldsets for presentation purposes.
 *
 * This code is adapted from views_ui_pre_render_add_fieldset_markup() but
 * designed to process arbitrary depths of nesting. It means that the form
 * presentation can modified from the form data structure, so the settings can
 * be presented in logical groups. To assign an an element to a fieldset create
 * the fieldset at the top level of the form as normal, and then assign the
 * #fieldset property to the individual form elements that will populate it.
 */
function swftools_pre_render_fieldsets($form) {
  return _swftools_pre_render_fieldsets($form, $form);
}

/**
 * Helper function for swftools_pre_render_fieldsets()
 *
 * Note that the function is called with the form as a parameter TWICE. One
 * copy is preserved to verify if the fieldset exists, and the second is used
 * to track the iterations and prepare the modified form.
 */
function _swftools_pre_render_fieldsets($form, $element, $recursed = FALSE) {

  // Initialise an array of fieldsets
  $fieldsets = &drupal_static(__FUNCTION__, array());

  // Iterate over the form/element being process on this pass
  foreach (element_children($element) as $key) {

    // If the form/element has children recurse in to them
    if (element_children($element[$key])) {
      $element[$key] = _swftools_pre_render_fieldsets($form, $element[$key], TRUE);
    }

    /**
     * If the element has the #fieldset property, and the fieldset exists then
     * move the element in to the fieldset array
     */
    if (isset($element[$key]['#fieldset']) && isset($form[$element[$key]['#fieldset']])) {
      $fieldsets[$element[$key]['#fieldset']][] = $element[$key];
      unset($element[$key]);
    }
  }

  // If this is the top level (not recursed in to) merge the fieldsets in
  if (!$recursed) {
    $element = swftools_array_merge($element, $fieldsets);
  }

  // Return the modified form/element
  return $element;

}

/**
 * Flushes all caches when new settings are stored.
 *
 * This function is called by player and embedding modules as a
 * submit handler to ensure all cached content is purged.
  */
function swftools_admin_settings_submit($form, &$form_state) {
  drupal_flush_all_caches();
}
