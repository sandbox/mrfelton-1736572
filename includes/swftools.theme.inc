<?php

/**
 * @file
 * Implements SWF Tools theme functions.
 *
 * For convenience all the SWF Tools theme functions are collected in this file
 * to make debugging easier.
 */

/**
 * @addtogroup swftools
 * @{
 */

/**
 * Turns an SWF Tools element in to markup for inclusion on the page,
 * using W3C compliant HTML.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function theme_swftools_direct($variables) {

  // Extract the element
  $element = $variables['element'];

  // Expand #file
  $file = file_create_url($element['#file']);

  // Strip out any parameters that do not have an actual value
  $element['#parameters'] = array_filter($element['#parameters']);

  // Turn parameters array in to a string
  $params = '';
  foreach ($element['#parameters'] as $key => $value) {
    $params .= '<param name="' . check_plain($key) . '" value="' . check_plain($value) . '" />';
  }

  // Turn flashvars array in to a string, and attach this to the parameters string
  $params .= $element['#flashvars'] ? '<param name="flashvars" value="' . swftools_http_build_query($element['#flashvars']) . '" />' : '';

  // Construct the embedding markup
  $attributes['outer'] = array(
    'id' => $element['#id'],
    'classid' => 'clsid:D27CDB6E-AE6D-11cf-96B8-444553540000',
    'height' => (string)$element['#height'],
    'width' => (string)$element['#width'],
  );
  $attributes['inner'] = array(
    'name' => $element['#id'],
    'type' => 'application/x-shockwave-flash',
    'data' => $file,
    'height' => (string)$element['#height'],
    'width' => (string)$element['#width'],
  );

  $html  = '<object' . drupal_attributes($attributes['outer']) . '>';
  $html .= '<param name="movie" value="' . $file . '" />';
  $html .= $params;
  $html .= '<!--[if gte IE 7]>-->';
  $html .= '<object' . drupal_attributes($attributes['inner']) . '>';
  $html .= $params;
  $html .= '<!--<![endif]-->';
  $html .= theme('swftools_html_alt', $element);
  $html .= '<!--[if gte IE 7]>-->';
  $html .= '</object>';
  $html .= '<!--<![endif]-->';
  $html .= '</object>';

  return $html;

}

/**
 * Themes the alternate HTML markup for an SWF Tools element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function theme_swftools_html_alt($variables) {

  $element = $variables['element'];

  /**
   * If nothing was specifically assigned on 'html_alt' then use the default
   * string from the settings page.
   */
  if ($element['#html_alt'] == NULL) {
    $element['#html_alt'] = variable_get('swftools_html_alt', SWFTOOLS_DEFAULT_HTML_ALT);
  }

  // @todo Make the filter used here definable
  return filter_xss_admin($element['#html_alt']);

}

/**
 * Applies a <div> wrapper around an SWF Tools element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function theme_swftools_wrapper($variables) {
  $element = $variables['element'];
  return '<div' . drupal_attributes($element['#attributes']) . '>' . $element['#children'] . '</div>';
}

/**
 * Builds a list of accessible controls for the specified player.
 *
 * SWF Tools accessibility scripts use classes of the form
 * !player-accessible-!action-!id. The array keys for accessibility controls
 * should be placed under $variables['othervars']['accessible'].
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #id, #accessible, #player
 */
function theme_swftools_accessible_controls($variables) {
  $element = $variables['element'];
  if (!$element['#accessible']['visible']) {
    return '';
  }
  foreach ($element['#accessible']['actions'] as $action => $label) {
    $list[] = l($label, '', array('fragment' => 'swftools-' . $element['#player']['name'] . '-' . $action, 'external' => TRUE, 'attributes' => array('class' => t('!player-accessible-!action-!id', array('!player' => $element['#player']['name'], '!action' => $action, '!id' => $element['#id'])))));
  }
  return theme_item_list(array('items' => $list, 'title' => '', 'type' => 'ul', 'attributes' => array('class' => $element['#accessible']['visible'] == SWFTOOLS_ACCESSIBLE_VISIBLE ? '' : 'swftools-accessible-hidden')));
}

/**
 * @} End of "addtogroup swftools"
 */
