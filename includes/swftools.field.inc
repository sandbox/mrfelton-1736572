<?php

/**
 * @file
 * Implements SWF Tools support for fields.
 *
 * The functions in this file allow SWF Tools to present itself as a formatter
 * for fields, and to process the resulting data ready for rendering via
 * theme_swftools().
 */

/**
 * Implements hook_field_formatter_info().
 */
function swftools_field_formatter_info() {
  return array(
    'swftools_separate' => array(
      'label' => t('SWF Tools - separate files'),
      'field types' => array('file', 'text'),
    ),
    'swftools_playlist' => array(
      'label' => t('SWF Tools - playlist'),
      'field types' => array('file', 'text'),
    ),
    'swftools_thumbnails' => array(
      'label' => t('SWF Tools - thumbnail'),
      'field types' => array('file', 'text'),
    ),

    /**
     * This is a special formatter for internal use only, so we don't assign
     * any field types. The formatter is available if we specifically ask for
     * it, but it is invisible to the user.
     */
    'swftools_plain' => array(
      'label' => t('SWF Tools - raw file path'),
      'field types' => array(),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function swftools_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();

  // Get the name of the key that holds the path to the content.
  $field_map = swftools_get_field_map($field['type']);

  // Prepare the element(s) for rendering.
  switch ($display['type']) {

    // Output unaltered file paths (used by Views)
    case 'swftools_plain':
      foreach ($items as $delta => $item) {
        $element[$delta] = array('#markup' => $item[$field_map['file']]);
      }
      break;

    // Output each item on its own by creating an array of elements
    case 'swftools_separate':
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#type' => 'swftools',
          '#data' => $item,
          '#file' => $item[$field_map['file']],
          '#field_map' => $field_map,
          '#delta' =>$delta,
        );
      }
      break;

    // Output the items all together in a playlist
    case 'swftools_playlist':
      $element[0] = array(
        '#type' => 'swftools',
        '#field_map' => $field_map,
        '#playlist' => $items,
      );
      break;

    // Cache thumbnails for use later
    case 'swftools_thumbnails':
      drupal_static_reset('swftools_thumbnails');
      foreach ($items as $delta => $item) {
        swftools_thumbnails(array('file' => $item[$field_map['file']], 'delta' => $delta));
      }
      return;

  }

  return $element;
}

/**
 * Implements hook_field_formatter_swftools().
 *
 * This function tells SWF Tools how to map data associated with a specific
 * field type to the elements of an SWF Tools playlist.
 *
 * Type: file
 * - uri (fid, display, description, uid, filename, filemime, filesize,
 *   status, timestamp, rdf_mapping)
 *
 * Type: text
 * - value (format, safe_value)
 */
function swftools_field_formatter_swftools() {
  return array(
    'file' => array('file' => 'uri', 'title' => 'description'),
    'text' => array('file' => 'value'),
  );
}

/**
 * Returns the name of the keys in a field definition that hold relevant data
 * for use in an SWF Tools playlist.
 *
 * To output content that has been generated via a field SWF Tools needs to
 * know which keys of the field data array contain data that can be used in
 * the playlist.
 *
 * Modules can implement hook_field_formatter_swftools() to return an array
 * of type/key pairs.
 *
 * The function should return an array with the field name as the key, and a
 * mapping array as the value. Each item in the mapping array is a key/value
 * pair where the key is the SWF Tools playlist element, and the value is the
 * corresponding data that should be used from the field. As a minimum the
 * array must contain the 'file' key.
 *
 * @param string $type
 *   The field type for which the mapping is requested.
 *
 * @return mixed
 *   The key of the array element that holds the path data, or FALSE if the
 *   key for that type isn't known.
 */
function swftools_get_field_map($type) {
  $uri_keys = &drupal_static(__FUNCTION__);
  if (!isset($uri_keys)) {
    if ($cache = cache_get('uri_keys', 'cache_swftools')) {
      $uri_keys = $cache->data;
    }
    else {
      $uri_keys = array();
      foreach (module_implements('field_formatter_swftools') as $module) {
        $result = module_invoke($module, 'field_formatter_swftools');
        if (isset($result) && is_array($result)) {
          $uri_keys = array_merge($uri_keys, $result);
        }
      }
      cache_set('uri_keys', $uri_keys, 'cache_swftools');
    }
  }
  return isset($uri_keys[$type]) ? $uri_keys[$type] : FALSE;
}

/**
 * Stores and retrieves thumbnail images.
 *
 * This function is used to cache images from fields formatted with the
 * SWF Tools thumbnail formatter, and to return them when the content is
 * actually rendered.
 *
 * @param array $variables
 *   Associative array with the following keys:
 *   - file: The path to the image file
 *   - delta: The delta of the image - required when storing, optional when
 *     retrieving
 *   - retrieve: (optional) When TRUE retrieve image(s)
 */
function swftools_thumbnails($variables) {

  // Initialise retrieve and delta keys
  $variables += array(
    'retrieve' => FALSE,
    'delta' => NULL,
  );

  // Create a static variable to hold the image path
  $images = &drupal_static(__FUNCTION__, array());

  /**
   * If retrieving previous thumbnails we may want to retrieve all cached
   * thumbnails (delta = NULL), or a specific thumbnail in which case delta is
   * the one we want. If retrieving a specific thumbnail just return its path,
   * or if retrieving all thumbnails return the entire array.
   */
  if ($variables['retrieve']) {
    if ($variables['delta'] !== NULL) {
      if (isset($images['#playlist'][$variables['delta']])) {
        return $images['#playlist'][$variables['delta']]['image'];
      }
      else {
        return '';
      }
    }
    else {
      return $images;
    }
  }

  /**
   * If file is empty then add an empty element to the array. Note that the
   * array construct looks a little strange, but we do it this way so that when
   * making a playlist we can simply merge this array with the SWF Tools
   * element and get all the image paths straight in to the right place.
   */
  if (!$variables['file']) {
    $images['#playlist'][$variables['delta']]['image'] = '';
    return;
  }

  // Add the result to the array
  $images['#playlist'][$variables['delta']]['image'] = swftools_build_uri($variables['file']);

  // Return nothing at this point
  return;

}
