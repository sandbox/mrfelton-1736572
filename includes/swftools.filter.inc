<?php

/**
 * @file
 * Implements SWF Tools support for filters.
 */

function swftools_filter_info() {
  $filters['swftools_filter'] = array(
    'title' => t('SWF Tools filter'), 
    'description' => t('Substitutes [swf file="filename.flv"] or [swf files="file1.jpg&&file2.jpg"] with embedding code.'), 
    'process callback' => '_swftools_filter_process_text', 
    'tips callback' => '_swftools_filter_process_tips',
  );
  return $filters;
}


/*
 * Return tips for swftools module
 */
function _swftools_filter_tips($delta, $format, $long = FALSE) {
  if ($long) {
    return ' 
    <h3 id="swftools_filter">' . t('SWF Tools Filter') . '</h3>
    <p>' . t('The basic syntax for embedding a flash file (.swf), flash movie (.flv) or audio file (.mp3) is') . ':</p>
    <blockquote><code>[swf file="filename.swf"]</code></blockquote>

    <p>' . t('If you would like to override SWF Tools and flash player default settings,
       you can specify additional parameters. For example') . ':</p>
    <blockquote><code>[swf file="song.mp3" flashvars="backcolor=#AABBCC&&forecolor=#11AA11"]</code></blockquote>
    <p>' . t('If you would like to output a list of files then the format is') . ':</p>
    <blockquote><code>[swf files="image1.jpg&&image2.jpg&&..."]</code></blockquote>
    ' . t('SWF Tools Filter will accept following') . ':
    <ul>
      <li><b>params</b> : ' . t('You can specify values for parameters to be passed to Flash
                           to control the appearance of the output. Typical values are
                           bgcolor and wmode. Example') . ': <code>params="wmode=true&&bgcolor="#00FF00"</code>
                           ' . t('Alternatively you can supply each parameter individually without using') .
                           '<code>params</code>. Example <code>wmode="true" bgcolor="#00FF00"</code></li>
      <li><b>flashvars</b> : '. t('You can specify values for output as flashvars, which
                           become available to the Flash movie that is playing. This is often done
                           to control a media player. Refer to the documentation of the flash player
                           you are using to know what flashvar options are available.
                           Example') . ': <code>flashvars="autostart=true&&volume=80"</code></li>
      <li><b>methods</b> : ' . t('Optional information about how to display the file. The most
                           common usage is to specify a particular media player and
                           thus override the default specified on the settings page.
                           Example') . ': <code>methods="player=onepixelout_mp3"</code></li>
       </ul>
      <p><strong>WARNING</strong>: '. t('with') . ' params, flashvars and othervars, ' . t('pass multiple values
                      separated by') . ' <strong>&amp;&amp;</strong>.</p>';
  }
  else {
    return t('You may use !swftools_filter_help to display Flash files inline', array("!swftools_filter_help" => l('<swf file="song.mp3">', "filter/tips/$format", array('query' => 'swftools_filter'))));
  }
}
/*
 * This function processes the filter text that the user has added to the text area.
 * If the filter is wrapped in <p></p> then these are stripped as part of the processing
 * This eliminates a validation error in the resulting mark up if SWF Tools filter is
 * being used in conjunction with other HTML filters that correct line breaks.
 * It won't work in EVERY case, but it will work in MOST cases.
 * Filters that are embedded in-line with text will continue to fail validation.
 */
function _swftools_filter_process_text($text) {
  if (preg_match_all('@(?:<p>)?\[(swflist|swf)\s*(.*?)\](?:</p>)?@s', $text, $match)) {
    // $match[0][#] .... fully matched string <swf|swflist parm_0="value_0" parm_1="value_1" parm_2="value_2">
    // $match[1][#] .... matched tag type ( swf | swflist )
    // $match[2][#] .... full params string until the closing '>'
    
    $swftools_parameters = array('file', 'params', 'flashvars', 'othervars', 'methods', 'files');
    $match_vars = array();
    foreach ($match[2] as $key => $passed_parameters) {
      //preg_match_all('/(\w*)=\"(.*?)\"/', $passed_parameters, $match_vars[$key]);
      preg_match_all('/(\w*)=(?:\"|&quot;)(.*?)(?:\"|&quot;)/', $passed_parameters, $match_vars[$key]);
      // $match_vars[0][#] .... fully matched string
      // $match_vars[1][#] .... matched parameter, eg flashvars, params
      // $match_vars[2][#] .... value after the '='
      
      // Process the parameters onto the $prepared array.
      // Search for standard parameters, parse the values out onto the array.
      foreach ($match_vars[$key][1] as $vars_key => $vars_name) {

        // Switch to swf or swflist, based on file or files
        // Need to tidy up this line, probably use switch/case
        if ($vars_name == 'file') {
          $match[1][$key] = 'swf';
        }
        else {
          if ($vars_name == 'files') {
            $match[1][$key] = 'swflist';
          }
        }

        if ($vars_name == 'file') {
          $prepared[$key][$vars_name] = $match_vars[$key][2][$vars_key];
          unset($match_vars[$key][1][$vars_key]);
        }
        elseif (in_array($vars_name, $swftools_parameters)) {
          $prepared[$key][$vars_name] = swftools_url_parse(str_replace(array('&amp;&amp;', '&&'), '&', $match_vars[$key][2][$vars_key]));
          unset($match_vars[$key][1][$vars_key]);
        }
        else {
          $prepared[$key]['othervars'][$vars_name] = $match_vars[$key][2][$vars_key];
        }
      }

      // Search for remaining parameters, map them as elements of the standard parameters.
      if (isset($prepared[$key]['methods']['player'])) {
        $player = strtolower($prepared[$key]['methods']['player']);
      }
      else {
        $player_key = array_search('player', $match_vars[$key][1]);
        if ($player_key!==FALSE) {
          $player = strtolower($match_vars[$key][2][$player_key]);
        }
        else {
          $player = FALSE;
        }
      }
      $prepared[$key]['methods']['player'] = $player;
      if (count($match_vars[$key][1])) {
        // Find out if a player has been set.
        foreach ($match_vars[$key][1] as $vars_key => $vars_name) {
          if ($parent = swftools_get_filter_alias($vars_name, $player)) {
            if ($parent) {
              $prepared[$key][$parent][$vars_name] = $match_vars[$key][2][$vars_key];
            }
          }
        }
      }

      // Just assigning parameters as false if not already set on the $prepared array.
      // Really just to avoid declaration warnings when we call swf and swf_list
      if (count($prepared[$key])) {
        foreach ($swftools_parameters AS $swfparameter) {
          if (!isset($prepared[$key][$swfparameter])) {
            $prepared[$key][$swfparameter] = FALSE;
          }
        }
      }

      // Assemble in to an array of options ready to pass
      $options = array();
      $options['params'] = $prepared[$key]['params'];
      $options['flashvars'] = $prepared[$key]['flashvars'];
      $options['othervars'] = $prepared[$key]['othervars'];
      $options['methods'] = $prepared[$key]['methods'];

      // Set a flag to show if we need to determine an action, or if one was provided
      $get_action = TRUE;
      if (isset($options['methods']['action'])) {
        $get_action = FALSE;
      }
      
      switch ($match[1][$key]) {
        case 'swf':
          $replace = swf($prepared[$key]['file'], $options);
          break;
        case 'swflist':
          $replace = '<!-- Playlist functionality not implemented -->';
          break;
      }
      $matched[] = $match[0][$key];
      $rewrite[] = $replace;
    }
    return str_replace($matched, $rewrite, $text);
  }
  return $text;
}

function swftools_url_parse($string) {
  $return = array();
  $pairs = explode("&", $string);
  foreach ($pairs as $pair) {
    $splitpair = explode("=", $pair);
    //if(!$splitpair[1] || array_key_exists($splitpair[0], $return)) {
    if (!isset($splitpair[1]) || array_key_exists($splitpair[0], $return)) {
      $return[] = $splitpair[0];
    }
    else {
      $return[$splitpair[0]] = $splitpair[1];
    }
  }
  return $return;
}

/*
 * This implements a hook that extends the parameters that can be passed to the filter
 * so that myvar="value" can be mapped to flashvars, etc.
 *
 */
function swftools_get_filter_alias($var, $player = FALSE) {

  static $general_mapping = array();
  static $player_mapping = array();

  if (!count($general_mapping)) {
    // Build up the mapping arrays.
    $general_mapping = array(
      'action'            => 'methods',
      'embed'             => 'methods',
      'width'             => 'params',
      'height'            => 'params',
      'swliveconnect'     => 'params',
      'play'              => 'params',
      'loop'              => 'params',
      'menu'              => 'params',
      'quality'           => 'params',
      'scale'             => 'params',
      'align'             => 'params',
      'salign'            => 'params',
      'wmode'             => 'params',
      'bgcolor'           => 'params',
      'base'              => 'params',
      'version'           => 'params',
      'allowfullscreen'   => 'params',
      'allowscriptaccess' => 'params',
    );
    if (!count($player_mapping)) {
      $player_mapping = module_invoke_all('swftools_variable_mapping');
    }
    $combined = array();
    if (count($player_mapping)) {
      foreach ($player_mapping AS $mapping) {
        $combined = array_merge($combined, $mapping);
      }
      $general_mapping = array_merge($combined, $general_mapping);
    }
  }
  // Return the parent of the variable.
  if ($player && isset($player_mapping[$player][$var])) {
      return $player_mapping[$player][$var];
  }
  else {
    return (isset($general_mapping[$var])) ? $general_mapping[$var] : FALSE;
  }
}