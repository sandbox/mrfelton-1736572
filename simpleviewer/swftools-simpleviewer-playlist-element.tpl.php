<?php

/**
 * @file
 * Template for a single entry in a SimpleViewer playlist.
 */
?>
  <image imageURL="<?php print $file; ?>">
    <caption><?php print $title; ?></caption>
  </image>
