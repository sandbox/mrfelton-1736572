<?php

/**
 * @file
 * Template for the xml wrapper around a SimpleViewer playlist.
 */
?>
<?xml version="1.0" encoding="UTF-8"?>
<simpleviewergallery<?php print $header; ?>>
<?php print $xml; ?>
</simpleviewergallery>
